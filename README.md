# Color icon in application menu

This simple GNOME Shell extension replaces the current application symbolic icon with the standard icon, bringing back some color in panel.

![screenshot](screenshot.png)

**Manual install**

    $ git clone https://gitlab.com/yanbab/gnome-shell-extension-regular-appmenu-icon.git ~/.local/share/gnome-shell/extensions/appmenu-color-icon@yanbab.gitlab.com

**Manual uninstall**

    $ rm -rf ~/.local/share/gnome-shell/extensions/appmenu-color-icon@yanbab.gitlab.com
